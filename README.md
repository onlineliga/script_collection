# OL Script Collection

This repository contains a collection of scripts for the online football manager game [Onlineliga][ol_link].

## Getting started

### Prerequisites

`Python 3.8` or higher is required to run the scripts. You can download it from the [official website][python_link].
It is possible that the scripts will run with older versions of Python, but this collection was developed with Python
3.8.

Also make sure you have `pip` installed with following packages:

- `aiohttp`
- `asyncio`
- `beautifulsoup4`
- `lxml`
- `XlsxWriter`
- `tqdm`
- `PyYAML`

You can simply install them by running the following command in the root directory of the repository:

```bash
pip install -r requirements.txt
```

### Configuration

In a file called `config.json` in the `configuration_files` directory of the repository you can set how many requests
are to be dialed simultaneously with `concurrent_requests` and in case of hitting the limit wait
for the amount of `lock_delay`

```json
{
    "concurrent_requests": 5,
    "lock_delay": 1
}
```

### Credentials

Some scripts require credentials to be set in a file called `credentials.json`. This file is located in
the `configuration_files` directory and looks like this:

```json
{
    "credentials": [
        {
            "login": "user@mail.org",
            "password": "xyz",
            "location": "onlineleague.co.uk"
        }
    ]
}
```

You can specify multiple credentials in this file. The login script will determine which credentials to use based on the
location.

If there is no file called `credentials.json` in the `configuration_files` directory, the scripts will create one with
default values. You can then edit the file and fill in your credentials.

If you do not want to store your credentials in a file, you will be prompted to enter them when you run the script.

## Scripts

### Training

This script will log into OL, retrieve player information and fitness levels, group the players
based on the training schedule, and set the groups in OL.

[Training Docs](docs/training.md) for a detailed documentation of the training script.

### Team Player Scraper

This script will scrape player data for specific teams and save it in CSV format. It was designed for
community-organized
tournaments, so its features are limited.

[Team Player Scraper Docs](docs/team_player_scraper.md) for a detailed documentation of the team player scraper script.

### Transfer History Player

This script will scrape the transfer history of youth players for a given range of player IDs and save it in an Excel
file.

[Transfer History Player Docs](docs/transferhistory_player.md) for a detailed documentation of the transfer history
player script.

**_Note:_** You will only receive data for players that came through the youth academy.

### Transfer History Team

This script will scrape the transfer history of teams for a given range of user IDs and save it in an Excel file.

[Transfer History Team Docs](docs/transferhistory_team.md) for a detailed documentation of the transfer history team
script,

### Match Stats Scraper

This script will scrape match stats for a given range of match IDs in a given season and save it in an Excel file.

[Match Stats Scraper Docs](docs/match_stats_scraper.md) for a detailed documentation of the match stats scraper script,

### League Scraper

This script will scrape league statistics, including team rankings, match results, and more from the

[League Scraper Docs](docs/league_scraper.md) for a detailed documentation of the league scraper script.

### Auto Bidder

This script will automatically bid on players in the transfer market based on a set of rules.

[Auto Bidder Docs](docs/auto_bidder.md) for a detailed documentation of the auto bidder script.

### Scan Market

This script will scan the player transfer market for specific player offers based on custom criteria.

[Scan Market Docs](docs/scan_market.md) for a detailed documentation of the scan market script.


### Auto Accept Friendly

This script will automatically accept friendly match requests based on a set of rules.

[Auto Accept Friendly Docs](docs/auto_friendly.md) for a detailed documentation of the auto accept friendly script.

## Docker

You can also run the scripts in a Docker container. To do so, you need to build the image first:

```bash
docker build -t ol_scripts .
```

Then you can run the scripts like this:

```bash
docker run --rm --name ol-script-collection \
    -v $(pwd)/configuration_files:/ol/configuration_files \
    -v $(pwd)/output:/ol/output \
    --user $(id -u):$(id -g) \
    ol_scripts league_scraper
```

[ol_link]: https://www.onlineliga.de "Onlineliga"

[python_link]: https://www.python.org/downloads/ "Python Download"
