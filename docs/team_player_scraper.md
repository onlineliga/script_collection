# Team Player Scraper Script

This Python script is designed to scrape player data from the OL website for community-organized tournaments. It allows
you to fetch important information about players from specific teams and save it in a CSV file for further analysis.

## Prerequisites

Before running the script, make sure you have the necessary dependencies installed.

This script requires the onlineliga web scraper this is an executable file. Unfortunately this project is no longer
being
maintained. You can find the source code [here][ols-source-code] and compile it yourself.

## Usage

### Command-line Arguments

The script accepts the following command-line arguments:

- `-l, --location`: The Online Liga website location (DE/AT/CH/UK). If not provided, it will prompt you to select a
location.
- `-t, --team-ids`: A list of team IDs for which you want to scrape player data. You can provide multiple team IDs
separated by spaces.
- `-o --output`: Path to the output directory. (Default: "output")

### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/team_player_scraper.py -l <location> -t <team_id_1> <team_id_2> ...
```

Replace `<location>` with the Online Liga location (e.g., onlineliga.de) and `<team_id_1>` `<team_id_2>` ... with the
IDs of
the teams you want to scrape player data for.

If no location is provided, the script will prompt you to select a location.

### Output
The script will scrape player data for the specified teams and save it in CSV format. The CSV file will contain the
following player information:

- Team ID
- Player ID
- Player Name
- Overall Rating
- Shooting Ability
- Power Ability
- Keeper on the Line Ability
- Keeper Box Ability
- Keeper Foot Ability
- Player Position

The CSV file will be named based on the location and timestamp of the script execution, e.g.,
`team_players_de_2023-09-08_15-30-45.csv`.

### Example Usage
Here's an example of how to run the script to scrape player data for two teams (Team A and Team B) in the "
onlineliga.de" location:

```bash
python onlineliga_script_collection/team_player_scraper.py -l onlineliga.de -t 1 2
```
This will generate a CSV file containing player data for Team A and Team B.

### Cleanup
The script creates a temporary directory to store scraped team data files. After the data is processed and saved to the
CSV file, the temporary directory is cleaned up automatically.

[ols-source-code]: https://gitlab.com/onlineliga/ols
