# Transfer History Player Script

This Python script is designed to scrape the youth player transfer history data from the OL website. It
allows you to specify a range of player IDs and retrieve their youth transfer history along with relevant player
details, such as name, positions, age, market value, overall rating, talent, and talent discovery date. The data is
saved in an Excel (XLSX) file for further analysis.

## Prerequisites

There are no special requirements for this script to run.

## Usage

### Command-line Arguments
The script accepts the following command-line arguments:

- `start`: The start player ID for the range of players you want to scrape.
- `end`: The end player ID for the range of players you want to scrape.
- `-l, --location`: The Online Liga website location (DE/AT/CH/UK). If not provided, it will prompt you to select a
  location.
- `-o --output`: Path to the output directory. (Default: "output")

### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/transferhistory_player.py <start> <end> -l <location>
```

Replace <start> and <end> with the player ID range you want to scrape, and <location> with the Online Liga location (
e.g., onlineliga.de).

### Output

The script will scrape youth player transfer history and player details for the specified range of player IDs. The data
will be saved in an Excel (XLSX) file with a filename based on the location and timestamp of the script execution, e.g.,
`youth_de_2023-09-08_15-30-45.xlsx`. The Excel file will contain the following columns:

- PlayerID
- Season
- Matchday
- TeamID
- PlayerName
- Positions
- Age
- BirthWeek
- Country
- MarketValue
- Overall
- Talent
- TalentDiscovered

The data can be used for various analysis and reporting purposes.

### Example Usage

Here's an example of how to run the script to scrape youth player transfer history for player IDs 1982621 to 1982631 in
the "
onlineliga.de" location:

```bash
python onlineliga_script_collection/transferhistory_player.py 1982621 1982631 -l onlineliga.de
```

This will generate an Excel file containing youth player transfer history and details for the specified range of player
IDs.
