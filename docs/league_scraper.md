# League Stats Scraper Script

This Python script is designed to scrape league statistics, including team rankings, match results, and more from the
OL (Online Liga) website. It allows you to specify a configuration file containing a dictionary of seasons and leagues
organized by location.

## Prerequisites

This script requires a valid configuration file containing a dictionary of seasons and leagues organized by location.

## Configuration Files

In order to run this script, you must provide a configuration file containing a dictionary of seasons and leagues.
This configuration file should be named `season_leagues.json` and should be located in the `configuration_files`.

`season_leagues.json`

Example season_leagues.json content:

```json
{
  "entries": [
    {
      "location": "onlineliga.de",
      "season_leagues": {
        "1": 2,
        "2": 2
      }
    },
    {
      "location": "onlineliga.at",
      "season_leagues": {
        "1": 2,
        "2": 2
      }
    }
  ]
}
```

### Season Leagues Explanation
This configuration file contains a list of entries. Each entry contains a location and a dictionary of seasons and
leagues. The location is the domain of the OL website, e.g., onlineliga.de. The seasons and leagues dictionary contains
season IDs as keys and league IDs as values.

The season ID is the number of the season.
The league ID is the number of the leagues you want to scrape.

In the example above, the script will scrape the league statistics for season 1 and 2 of the onlineliga.de and
onlineliga.at websites. It will scrape the statistics for the first two leagues of each season.

## Usage

### Command Line Arguments

- `-f --file`: Path to the configuration file containing the season-league dictionary. (Default: "
  configuration_files/season_leagues.json")
- `-o --output`: Path to the output directory. (Default: "output")

### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/league_scraper.py
```

### Example Usage

```bash
python league_stats_scraper.py -f configuration_files/season_leagues.json
```

This command will scrape league statistics based on the configurations provided in the specified JSON file.

### Output

This script will scrape league statistics based on the configurations in your JSON file and create a CSV file for each
location and season combination. The CSV files will contain the following league statistics:

- season
- league_id
- league_name
- league level
- placement
- team_id
- team_name
- matches
- wins
- draws
- losses
- goals_for
- goals_against
- goal_difference
- points

The CSV files will be named based on the location, season, and timestamp of the script
execution, e.g., league_stats_onlineliga.de_2023-09-08_15-30-45.csv.
