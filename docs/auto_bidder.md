# Auto Bidder Script

The Auto Bidder script allows you to automate placing bids on player transfers in OL. This script is designed to be used
with given OL servers and provides a convenient way to bid on player transfers with various parameters.

## Features

- Automated bidding on player transfers.
- Customizable bid parameters such as transfer fee, salary, and bid duration.
- Retrieves player information, including their name and offer end date.
- Checks if the offer is still valid before placing a bid.

## Prerequisites

There are no special prerequisites required to run this script.

## Usage

### Command Line Arguments

The script accepts the following command line arguments:

- `-l, --location`: The location of the online football league website. Choose from "onlineliga.de," "onlineliga.ch," "
onlineliga.at," or "onlineleague.co.uk."
- `-o, --offer_id`: The offer ID for the bid.
- `-d, --duration`: The bid duration in days.
- `-tf, --transfer_fee`: The transfer fee for the bid.
- `-s, --salary`: The salary for the bid.
- `-t, --target_time`: The target time in seconds to place the bid. If not specified, the script will place the bid
  15 seconds before the offer end date.


### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/auto_bidder.py -l <location> \
  -o <offer_id> \
  -d <duration> \
  -tf <transfer_fee> \
  -s <salary> \
    -t <target_time>
```

Replace <location> with the desired online league location, <offer_id> with the offer ID for the bid, <duration> with
the bid duration in days, <transfer_fee> with the transfer fee for the bid, <salary> with the salary for the bid and
<target_time> with the target time in seconds to place the bid.

### Example Usage

```bash
python onlineliga_script_collection/auto_bidder.py -l onlineliga.at -o 320962 -d 2 -tf 1000 -s 1000
```

This command will place a bid on a player transfer with the specified parameters on the "onlineliga.at" website.

## Workflow

The script retrieves player information, including their name and offer end date.
It checks if the offer is still valid based on the offer end date.
If the offer is valid, the script waits until the target time (offer end date) minus the specified duration is reached.
Once the target time is reached, the script places the bid with the specified parameters.

## Disclaimer

This script is intended for educational and informational purposes only. Use it responsibly and ensure compliance with
the terms of use of the online league website. The script does not guarantee successful bids and may require adjustments
based on website changes.
