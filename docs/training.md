# Training Schedule Script

This Python script automates the process of setting training groups for Online Liga (OL) based on player fitness and
positions. It parses player data, fitness information, and training group requirements to organize players into
appropriate groups for training.

## Prerequisites

This script requires login credentials for the OL website. The credentials should be stored in a file
called `credentials.json` in the `configuration_files` directory. Please refer to
the [Credentials](../README.md#credentials) section in the main README for more information.

## Configuration Files

`players.json`

This file contains a list of players with their respective IDs and positions. Players are grouped into training groups
based on their positions.

Example players.json content:

```json
{
    "players": [
        {
            "id": 1680644,
            "position": "ST"
        },
        {
            "id": 1735170,
            "position": "ST"
        }
    ]
}
```

`schedule.json`
This file defines the training schedule and the requirements for each training group. Training groups are organized by
name, fitness range, and required positions.

Requirements:
- `fitness`: The fitness range for players in the group. The range is defined by two numbers separated by a dash (-).
  The first number is the minimum fitness, and the second number is the maximum fitness. For example, a fitness range
  of 0-100 means that players with a fitness level between 0 and 100 (inclusive) will be placed in the group.
- `positions`: The positions that players in the group must have. The positions are defined by their abbreviations.
  For example, a position of "ST" means that players with the position "ST" will be placed in the group.
- `played`: Whether players in the group have played in a match. If this is set to `true`, then players who have
  played in a match will be placed in the group. If this is set to `false`, then players who have not played in a match
  will be placed in the group.

Example schedule.json content:

```json
{
    "id": 181007,
    "training_groups": [
        {
            "id": 499035,
            "name": "0000 alle",
            "requirements": [
                {
                    "fitness": "0-100"
                }
            ]
        },
        {
            "id": 499038,
            "name": "0001 alle ohne doppelt",
            "requirements": [
                {
                    "fitness": "0-100",
                    "positions": [
                        "IV",
                        "DM",
                        "AV",
                        "ST",
                        "OM",
                        "TW1",
                        "TW",
                        "TW2"
                    ]
                }
            ]
        },
        {
            "id": 499048,
            "name": "0002 doppelt",
            "requirements": [
                {
                    "fitness": "0-100",
                    "positions": [
                        "doppelt"
                    ]
                }
            ]
        },
        {
            "id": 499037,
            "name": "0003 A",
            "requirements": [
                {
                    "fitness": "0-100",
                    "positions": [
                        "ST",
                        "OM"
                    ]
                }
            ]
        },
        {
            "id": 499049,
            "name": "0004 B",
            "requirements": [
                {
                    "fitness": "0-100",
                    "played": false,
                    "positions": [
                        "DM",
                        "IV"
                    ]
                }
            ]
        }
    ]
}
```

## Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/training.py
```

The script will automatically organize players into training groups according to the defined requirements in
schedule.json.

### Running the Script with Custom File Paths

You can specify custom file paths for players.json and schedule.json using the -p (or `--players`) and -s (
or `--schedule`)
command-line arguments, respectively. Here's how to do it:

```bash
python onlineliga_script_collection/training.py -p /path/to/custom_players.json -s /path/to/custom_schedule.json
```

Replace `/path/to/custom_players.json` and `/path/to/custom_schedule.json` with the actual file paths of your custom
configuration files. The script will use these custom files to organize players into training groups based on the
provided data.

Example:

```bash
python training_schedule_script.py -p custom_players.json -s custom_schedule.json
```

## Output

The script will provide output indicating the success or failure of organizing players into training groups. If any
errors occur during the process, they will be displayed in the console.
