# Friendly Match Auto-Accept Script

The Friendly Match Auto-Accept script automates the process of accepting friendly match requests on OL websites.
This script provides various options to filter and accept friendly match requests based on your preferences.

## Features

- Automates the acceptance of friendly match requests.
- Customizable filtering options based on team ID, stadium capacity, league level, and more.
- Automatic login to the OL website.
- Supports multiple OL locations: DE, AT, CH, UK.

## Prerequisites

There are no special prerequisites required to run this script.

## Usage

### Command Line Arguments

The script accepts the following command line arguments:

- `-v, --verbose`: Increase output verbosity.
- `-l, --location`: Specify the OL location (DE, AT, CH, UK).
- `-w, --week`: The week of the season (default: current).
- `-s, --season`: The season of the league (default: current).
- `-d, --delay`: Amount of time in seconds to wait between checking for new friendly match requests (default: 10).
- `-t, --team`: Team ID of a specific team you want to play against (default: None).
- `-c, --capacity`: Minimum capacity of the stadium of the team you want to play against (default: 0).
- `-lmin, --league_level_min`: Minimum league level of the team you want to play against (default: 1).
- `-lmax, --league_level_max`: Maximum league level of the team you want to play against (default: 6).
- `-bt, --blacklisted_teams`: Comma-separated list of team IDs that you don't want to play against (default: None).

### Running the Script

To run the script, execute it using Python in the command line. Here's an example:

```bash
python auto_accept_friendly.py -l onlineliga.de -t 1234 -c 50000
```

## Workflow

The script logs in to the OL website, fetches the list of friendly match requests, and filters them based on
the provided criteria. It then attempts to accept the first matching friendly match request.

If the acceptance is successful, the script will print a success message and exit. If not, it will retry after a
specified delay.

## Disclaimer

This script is intended for educational and informational purposes only. Use it responsibly and ensure compliance with
the terms of use of the OL website. The script does not guarantee successful friendly match acceptances and
may require adjustments based on website changes.
