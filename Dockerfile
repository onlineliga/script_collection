FROM python:3.11.5-alpine

COPY ./onlineliga_script_collection /ol/scripts
COPY ./requirements.txt /ol
COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /ol

RUN pip install --no-cache-dir -r requirements.txt

RUN mkdir -p /ol/configuration_files \
    && mkdir -p /ol/output \
    && find /ol/scripts -type d -exec chmod 777 {} + \
    && find /ol/scripts -type f -exec chmod 755 {} +

ENTRYPOINT ["/entrypoint.sh"]
