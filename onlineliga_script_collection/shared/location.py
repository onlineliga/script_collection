import curses

LOCATIONS = ['onlineliga.de', 'onlineliga.at', 'onlineliga.ch', 'onlineleague.co.uk']


def select_location() -> str:
    # Initialize the curses library and set up terminal I/O
    stdscr = curses.initscr()
    curses.start_color()

    # Initialize a color pair with red text on a black background
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.cbreak()
    curses.noecho()

    # Enable keypad input
    stdscr.keypad(True)

    # Get the height and width of the terminal
    height, width = stdscr.getmaxyx()

    # Initialize variables to store the selected location and the current choice
    choice = 0

    # Loop until a location is selected
    while True:
        # Clear the terminal
        stdscr.clear()

        # Iterate over the list of locations and display them
        for index, item in enumerate(LOCATIONS):
            x = width // 2 - len(item) // 2
            y = height // 2 - len(LOCATIONS) // 2 + index
            if index == choice:
                stdscr.attron(curses.color_pair(1))
                stdscr.addstr(y, x, item)
                stdscr.attroff(curses.color_pair(1))
            else:
                stdscr.addstr(y, x, item)
        stdscr.refresh()

        # Get the next key press
        key = stdscr.getch()

        # Update the choice based on the key press
        if key == curses.KEY_UP:
            choice = max(choice - 1, 0)
        elif key == curses.KEY_DOWN:
            choice = min(choice + 1, len(LOCATIONS) - 1)
        elif key == ord('\n'):
            location = LOCATIONS[choice]
            break

    # Disable keypad input and restore terminal I/O
    curses.nocbreak()
    stdscr.keypad(False)
    curses.echo()
    curses.endwin()

    # Return the selected location
    return location
