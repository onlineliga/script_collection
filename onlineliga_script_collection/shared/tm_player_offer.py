class PlayerOffer:
    def __init__(self, player_id, player_name, player_positions, attributes, offer_details):
        self.player_id = player_id
        self.player_name = player_name
        self.player_positions = player_positions
        self.attributes = attributes
        self.offer_details = offer_details

    def __str__(self):
        return (f"PlayerID: {self.player_id}, "
                f"Name: {self.player_name}, "
                f"Positions: {self.player_positions}, "
                f"Attributes: {self.attributes}, "
                f"Offer Details: {self.offer_details}")

    def to_dict(self):
        return {
            "player_id": self.player_id,
            "player_name": self.player_name,
            "player_positions": self.player_positions,
            "attributes": self.attributes,
            "offer_details": self.offer_details
        }

    @staticmethod
    def apply_filter(player, filter_values):
        # Check additional filters if present
        if filter_values:
            for attr_name, attr_range in filter_values.items():
                if (
                    attr_name in player.attributes
                    and attr_range.get("min",
                                       float('-inf')) <= player.attributes[attr_name] <= attr_range.get("max",
                                                                                                        float('inf'))
                ):
                    continue
                else:
                    # If any filter does not match, return False
                    return False
        # If all filters match, return True
        return True
