from datetime import datetime

import bs4


def parse_attributes(attribute_list: dict, config_attributes: dict):
    attributes = {}
    key_map = config_attributes
    for key, value in key_map.items():
        search_value = str(value)
        if search_value in attribute_list:
            target_value_dict = attribute_list[search_value]
            target_value = target_value_dict["value"]
            attributes[key] = target_value
    return attributes


def remove_non_numeric(string: str) -> str:
    return ''.join(i for i in string.strip() if i.isdigit())


def parse_offer_details(offer_element: bs4.element):
    offer_details = {}

    try:
        if offer_element:
            offer_details["offerId"] = offer_element["data-offer-id"]
            highest_bid_element = offer_element.find(attrs={"class": "transfer-player-highestbid-column"})
            if highest_bid_element:
                highest_bid = remove_non_numeric(highest_bid_element.text)
                offer_details["highestBid"] = highest_bid
            num_bids_element = offer_element.find(attrs={"class": "transfer-player-bids-column"})
            if num_bids_element:
                num_bids = remove_non_numeric(num_bids_element.text)
                offer_details["numberOfBids"] = num_bids

    except KeyError as e:
        print(f"KeyError: {e}")
    except Exception as e:
        print(f"An error occurred: {e}")

    return offer_details


def parse_player_name(element: bs4.element):
    player_name = ""
    if element:
        player_name_element = element.find(attrs={"class": "ol-player-name"})
        if player_name_element:
            player_name = player_name_element.text
            # remove the leading and trailing whitespaces
            player_name = player_name.strip()
    return player_name


def parse_player_position(element: bs4.element):
    positions = []
    if element:
        positions_element = element.find(attrs={"class": "transfer-player-position-column"})
        if positions_element:
            positions_text = positions_element.text
            # if the player has multiple positions they are separated by a comma and a whitespace
            positions = positions_text.split(", ")
    return positions


def parse_end_date(element):
    end_date_element = element.find(attrs={"class": "transfer-auction-end-date"})
    if end_date_element:
        end_date = end_date_element["data-utc"]
        end_date = convert_timezone(end_date)
        return {"endDate": end_date}
    return {"endDate": "n/a"}


def parse_player_price(element, selector, keys, default_value=None, offset=0):
    additional_elements = element.select(selector)
    parsed_data = {}

    try:
        if additional_elements:
            for i, key in enumerate(keys):
                value = remove_non_numeric(additional_elements[i + offset].text)
                parsed_data[key] = value
        else:
            for key in keys:
                parsed_data[key] = default_value
    except Exception as e:
        print(f"An error occurred while parsing player price: {e}")
        for key in keys:
            parsed_data[key] = default_value

    return parsed_data


def parse_offer_id(element):
    offer_id_element = element.find(attrs={"class": "bid-button"})
    if offer_id_element:
        try:
            # example: "bid(12345678)"
            offer_id = offer_id_element["onclick"].split("(")[1].split(")")[0]
            # there are occasions where the example is like this: "bid(12345678 , false , 1)"
            # in this case we only want the first part and remove the rest
            try:
                offer_id = offer_id.split(",")[0].strip()
            except IndexError:
                pass
            return {"offerId": offer_id}
        except IndexError:
            return {"offerId": -1}
    return {"offerId": -1}


def parse_additional_offer_details(element):
    additional_offer_details = {}

    details_parsers = [
        parse_end_date,
        lambda e: parse_player_price(e, ".transfer-overview-player-price .strong-black",
                                     ["minimumFee", "suggestedSalary"], "n/a", 1),
        lambda e: parse_player_price(e, ".transfer-overview-player-price .strong-gehalt",
                                     ["maxOfferedSalary"], "n/a"),
        lambda e: parse_player_price(e, ".transfer-player-overview-font-small .strong-black",
                                     ["currentSalary"], "n/a", 1),
        parse_offer_id
    ]

    for parser in details_parsers:
        parsed_data = parser(element)
        additional_offer_details.update(parsed_data)

    return additional_offer_details


def convert_timezone(end_date):
    timezone = datetime.now().astimezone().tzinfo
    # convert to datetime object
    end_date = datetime.fromisoformat(end_date)
    # convert to user timezone
    end_date = end_date.astimezone(timezone)
    # convert to string
    end_date = end_date.strftime("%Y-%m-%d %H:%M:%S")
    return end_date
