#!/usr/bin/env python3

import getpass
import aiohttp
from bs4 import BeautifulSoup
from .credentials_parser import load_credentials


async def get_token(login_url: str, session: aiohttp.ClientSession):
    response = await session.get(login_url)
    data = await response.text()
    soup = BeautifulSoup(data, "html.parser")
    token = soup.find('input', {'name': '_token'})['value']
    return token


def parse_credentials(location):
    creds = load_credentials("configuration_files/credentials.json")
    try:
        if "credentials" not in creds:
            raise ValueError("The `credentials` key is not defined in the JSON")

        credentials = creds.get("credentials")

        # Filter the list of credentials to only include those with a matching location
        matching_credentials = [c for c in credentials if c.get("location") == location]

        # Check if there is at least one matching credential
        if not matching_credentials:
            raise ValueError(f"No credentials found for location: {location}")

        # Return the first matching credential
        return matching_credentials[0]
    except ValueError as e:
        print(f"Error: {e}")
        return None


def determine_login_params(token: str, location: str):
    credentials = parse_credentials(location)
    if credentials:
        # if secrets are present only add the auth token to parameters
        payload = credentials
        payload["_token"] = f"{token}"
    else:
        # if no secrets present prompt for id and password
        payload = {"_token": f"{token}",
                   "login": input("Whats your Username?"),
                   "password": getpass.getpass("Whats your Password?")}
    return payload


async def login(session: aiohttp.ClientSession, location: str) -> str:
    login_url = f"https://www.{location}/login"

    # Get the token
    token = await get_token(login_url, session)
    print(f"Session will be initiated with following token: {token}")

    # Determine the login parameters
    payload = determine_login_params(token, location)

    # Make the POST request to log in
    async with session.post(login_url, data=payload) as response:
        response_text = await response.text()
        print(f"Login response: {response_text}")
        # Check if the response is successful
        if len(response_text) != 0:
            raise Exception("Login failed. Please check your credentials.")
        else:
            print("Login successful!")
            return token
