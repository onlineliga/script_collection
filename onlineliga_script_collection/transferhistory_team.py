#!/usr/bin/env python3
import argparse
import json
import re
from argparse import Namespace
from datetime import datetime

import aiohttp
import asyncio
import time

from typing import List

import bs4
import xlsxwriter
from bs4 import BeautifulSoup
import sys
import tqdm

from shared.config_parser import config

start_time = time.time()

regex = re.compile("playerId")


def parse_arguments() -> Namespace:
    # arguments
    # generate a parser instance
    parser = argparse.ArgumentParser(description="scrape transfer history of a team in onlineliga",
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("-o", "--output",
                        type=str,
                        help="directory where the output files will be stored",
                        required=False,
                        default="output")

    args = parser.parse_args()
    return args


def parse_player_id(element: bs4.element) -> int:
    pid_regex = re.compile("playerId")
    player_id_elements = element.find_all(onclick=pid_regex)
    player_id_element = player_id_elements[0]
    player_id_target_text = player_id_element["onclick"]
    player_id = player_id_target_text.split("playerId: ", 1)[1].split(" }", 1)[0]
    return player_id


def parse_player_name(element: bs4.element) -> str:
    name_element = element.find(attrs={"class": "player-name bold"})
    return name_element.text.strip()


def parse_team_id(element: bs4.element):
    team_element = element.find(attrs={"class": "ol-team-name"})
    if not team_element:
        team_element = element.find(attrs={"class": "ol-team-name-inactive"})
    if team_element:
        team_id_attr = team_element["onclick"]
        player_id = team_id_attr.split("userId : ", 1)[1].split(" }", 1)[0]
        return player_id
    return ""


def parse_transfer_fee(element: bs4.element) -> str:
    fee_element = element.find(attrs={"class": "transferhistory-player-bought"})
    if not fee_element:
        fee_element = element.find(attrs={"class": "transferhistory-player-sold"})
    fee = strip_comma(fee_element.text)
    fee = strip_pound(fee)
    return fee


def strip_comma(fee_element_text):
    fee = fee_element_text.replace(',', '')
    return fee


def strip_pound(fee_element_text):
    fee = fee_element_text.replace(' £', '')
    return fee


def parse_position(element: bs4.element) -> str:
    age_element = element.find(attrs={"class": "transferhistory-player-age"})
    position_element = age_element.find_next()
    return position_element.text


def parse_age(element: bs4.element) -> int:
    age_element = element.find(attrs={"class": "transferhistory-player-age"})
    return age_element.text.strip()


def parse_transfer_date(element: bs4.element):
    date_element = element.find(attrs={"ol-timestamp-string"})
    return date_element.text.strip()


def flatten(xss):
    return [x for xs in xss for x in xs]


def write_xlsx(team_list, location: str, output_dir: str):
    # Pick a filename based on the current date and time and the location
    current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    file_name = f"{output_dir}/th_team_{location}_{current_time}.xlsx"
    flat_list = flatten(team_list)

    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()

    headers = ["VereinsID", 'Spielername', 'andererVerein', 'playerid', 'betrag', "position", "Alter", "zeitpunkt",
               "Saison", "Schlüssel"]

    for i, header in enumerate(headers):
        worksheet.write(0, i, header)

    for i, player in enumerate(flat_list):
        for j, detail in enumerate(player):
            worksheet.write(i + 1, j, detail)

    workbook.close()


def parse_players(team, user_id: int, season: int):
    players = []
    for element in team:
        player_name = parse_player_name(element)
        player_id = parse_player_id(element)
        team_id = parse_team_id(element)
        position = parse_position(element)
        age = parse_age(element)
        transfer_date = parse_transfer_date(element)
        transfer_fee = parse_transfer_fee(element)
        key = f"{season}{user_id}{team_id}{player_id}"
        players.append(
            [user_id, player_name, team_id, player_id, transfer_fee, position, age, transfer_date, season, key])
    return players


def read_input():
    with open("./configuration_files/team_transfers_input.json", "r") as file:
        data = json.load(file)
        return data["user_ids"], data["season"], data["location"]


async def fetch(session, url, user_id, season):
    async with session.get(url) as resp:
        page = await resp.text()
        soup = BeautifulSoup(page, "lxml")
        player_elements = soup.find_all(attrs={"class": "team-overview-history-table-row"})
        players = parse_players(player_elements, user_id, season)
        return players


async def bound_fetch(sem, session, url, user_id, season):
    async with sem:
        # When workers hit the limit, they'll wait for a second
        # before making more requests.
        if sem.locked():
            await asyncio.sleep(delay=config["lock_delay"])
        return await fetch(session, url, user_id, season)


async def fetch_all(user_ids: List[int], season: int, location: str, output_dir: str):
    async with aiohttp.ClientSession() as session:
        semaphore = asyncio.Semaphore(config["concurrent_requests"])

        tasks = []
        for user_id in user_ids:
            url = f'https://www.{location}/team/overview/transferhistory/season?userId={user_id}&season={season}'
            tasks.append(asyncio.ensure_future(bound_fetch(semaphore, session, url, user_id, season)))

        responses = []
        for f in tqdm.tqdm(asyncio.as_completed(tasks), total=len(tasks)):
            responses.append(await f)

        team_list = await asyncio.gather(*tasks)
        write_xlsx(team_list, location, output_dir)


def main():
    args = parse_arguments()
    if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    user_ids, season, location = read_input()
    asyncio.run(fetch_all(user_ids, season, location, args.output))
    print(f"--- %s seconds ---" % (time.time() - start_time))


if __name__ == '__main__':
    main()
