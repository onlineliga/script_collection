#!/usr/bin/env python3
import argparse
import asyncio
import csv
import sys
import time
from argparse import Namespace
from datetime import datetime

import aiohttp
import bs4
import tqdm
from bs4 import BeautifulSoup

from shared.location import select_location
from shared.config_parser import config

start_time = time.time()


def parse_arguments() -> Namespace:
    # arguments
    # generate a parser instance
    parser = argparse.ArgumentParser(description='scrape penalties in onlineliga',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-b ', '--begin', type=int, help='The start match ID.')
    parser.add_argument('-t ', '--to', type=int, help='The end match ID.')
    parser.add_argument('-s ', '--season', type=int, help='The season.')
    parser.add_argument('-l ', '--location', type=str, help='DE/AT/CH/UK', required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])
    parser.add_argument("-o", "--output",
                        type=str,
                        help="directory where the output files will be stored",
                        required=False,
                        default="output")

    args = parser.parse_args()
    if not args.location:
        args.location = select_location()
    return args


def parse_injuries(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    injuries = soup.find_all(attrs={"class": "icon-icon_cross_red"})
    return len(injuries)


def parse_system_changes(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    system_changes = soup.find_all(attrs={"class": "icon-icon_system-change"})
    return len(system_changes)


def parse_goals(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    goals = soup.find_all(attrs={"class": "icon-lineup_icon_goal_de"})
    return len(goals)


def parse_own_goals(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    own_goals = soup.find_all(attrs={"class": "icon-lineup_icon_own_goal_de"})
    return len(own_goals)


def parse_missed_penalties(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    missed_pens = soup.find_all(attrs={"class": "icon-icon_penalty_miss_small"})
    return len(missed_pens)


def parse_successful_penalties(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    successful_penalties = soup.find_all(attrs={"class": "icon-lineup_icon_penalty"})
    return len(successful_penalties)


def parse_substitutions(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    substitutions = soup.find_all(attrs={"class": "icon-lineup_icon_substitution"})
    return len(substitutions)


def parse_yellow_cards(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    yellow_cards = soup.find_all(attrs={"class": "icon-lineup_icon_yellow"})
    return len(yellow_cards)


def parse_yellow_red_cards(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    yellow_red_cards = soup.find_all(attrs={"class": "icon-lineup_icon_yellowred"})
    return len(yellow_red_cards)


def parse_red_cards(soup: bs4.BeautifulSoup) -> int:
    if soup is None:
        return 0
    red_cards = soup.find_all(attrs={"class": "icon-lineup_icon_red"})
    return len(red_cards)


def parse_not_a_match(soup: bs4.BeautifulSoup) -> str:
    if soup is None:
        return "MATCH UNAVAILABLE"
    return "OK"


async def fetch(session, url, match_id, season):
    async with session.get(url) as resp:
        page = await resp.text()
        soup = BeautifulSoup(page, "lxml")
        timeline_element = soup.find(attrs={"class": "timeline-wrapper"})
        missed_pens = parse_missed_penalties(timeline_element)
        successful_pens = parse_successful_penalties(timeline_element)
        goals = parse_goals(timeline_element)
        own_goals = parse_own_goals(timeline_element)
        system_changes = parse_system_changes(timeline_element)
        yellow_cards = parse_yellow_cards(timeline_element)
        yellow_red_cards = parse_yellow_red_cards(timeline_element)
        red_cards = parse_red_cards(timeline_element)
        substitutions = parse_substitutions(timeline_element)
        injuries = parse_injuries(timeline_element)
        status = parse_not_a_match(timeline_element)

        return [match_id, season, missed_pens, successful_pens, goals, own_goals, system_changes, yellow_cards,
                yellow_red_cards, red_cards, substitutions, injuries, status, url]


async def bound_fetch(sem, session, url, season, league):
    async with sem:
        # When workers hit the limit, they'll wait for a second
        # before making more requests.
        if sem.locked():
            await asyncio.sleep(delay=config["lock_delay"])
        return await fetch(session, url, season, league)


async def fetch_all(match_id_start: int, match_id_end: int, season, location: str, output_dir: str):
    async with aiohttp.ClientSession() as session:
        semaphore = asyncio.Semaphore(config["concurrent_requests"])

        tasks = []
        for match_id in range(match_id_start, match_id_end):
            url = f"https://www.{location}/match/statistic?season={season}&matchId={match_id}"
            tasks.append(asyncio.ensure_future(
                bound_fetch(semaphore, session, url, match_id, season)))

        responses = []
        for f in tqdm.tqdm(asyncio.as_completed(tasks), total=len(tasks)):
            responses.append(await f)

        match_list = await asyncio.gather(*tasks)

        current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name = f"{output_dir}/match_stats_{location}_{current_time}.csv"

        complete_file = open(f"{file_name}", "w", newline="", encoding="utf-8-sig")
        writer = csv.writer(complete_file)
        writer.writerow(["match_id", "season", "missed_pens", "successful_pens", "goals", "own_goals", "system_changes",
                         "yellow_cards", "yellow_red_cards", "red_cards", "substitutions", "injuries", "status", "url"])
        writer.writerows(match_list)
        complete_file.close()


def main():
    args = parse_arguments()
    if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(fetch_all(args.begin, args.to, args.season, args.location, args.output))
    print(f"--- %s seconds ---" % (time.time() - start_time))


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        time_elapsed = time.time() - start_time
        print("Keyboard interrupt detected. Exiting...")
        print(f"--- {time_elapsed} seconds ---")
        exit(0)
