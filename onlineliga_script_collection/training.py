#!/usr/bin/env python3
import argparse
import asyncio
import json
import re
import urllib.parse
from argparse import Namespace

import aiohttp
import bs4
from bs4 import BeautifulSoup

from shared.location import select_location
from shared.player_id_parser import parse_training_player_id

from shared.login import login


def parse_arguments() -> Namespace:
    # arguments
    # generate a parser instance
    parser = argparse.ArgumentParser(description='set training groups in OL',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-l ', '--location', type=str, help='DE/AT/CH/UK', required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])
    parser.add_argument('-p ', '--players', type=str, help='players.json', default="players.json")
    parser.add_argument('-s ', '--schedule', type=str, help='training_schedule.json', default="training_schedule.json")

    args = parser.parse_args()

    if not args.location:
        args.location = select_location()
    return args


async def handle_tg_post_response(response):
    if response.status != 200:
        print(f"Error: POST request failed with status code {response.status}")
        return

    response_text = await response.text()
    try:
        response_json = json.loads(response_text)
        error = response_json.get("err")
        if error:
            print(f"Error occurred while posting data: {error}")
        else:
            print(f"Data posted successfully: {response_json}")
    except json.JSONDecodeError:
        print(f"Error: Failed to parse response as JSON: {response_text}")


def get_training_schedule_details(file_name) -> (int, list):
    with open(file_name, "r") as file:
        data = json.load(file)

    schedule_id = data.get("id")
    training_groups = data.get("training_groups")

    return schedule_id, training_groups


def get_players_test(file_name) -> list:
    with open(file_name, "r") as file:
        data = json.load(file)

    players = data.get("players")

    return players


def group_players_by_training_requirements(training_groups, players):
    # Initialize an empty dictionary to store the players grouped by their training requirements
    grouped_players = {}

    # Loop through all the training groups
    for training_group in training_groups:
        # Get the requirements of the current training group
        requirements = training_group.get("requirements")

        # Initialize a list to store the players who match the requirements of the current training group
        matching_players = []

        # Loop through all the players
        for player in players:
            # Loop through the requirements of the current training group
            for requirement in requirements:
                # Check if the player matches the position requirement
                if not matches_position(player, requirement):
                    # If the player doesn't match the position requirement, move on to the next player
                    continue

                # Check if the player matches the played requirement
                if not matches_played(player, requirement):
                    # If the player doesn't match the played requirement, move on to the next player
                    continue

                # Check if the player matches the fitness requirement
                if not matches_fitness(player, requirement):
                    # If the player doesn't match the fitness requirement, move on to the next player
                    continue

                # If the player matches all the requirements, add them to the list of matching players
                matching_players.append(player)

        # Add the list of matching players to the `grouped_players` dictionary, with the key being the `id` of the
        # current training group
        grouped_players[training_group.get("id")] = matching_players

    # Return the `grouped_players` dictionary
    return grouped_players


def matches_fitness(player, requirement):
    """
    Determine if a player matches the fitness requirement defined in the training group.

    :param player: A dictionary representing a player and their attributes
    :param requirement: A dictionary representing a requirement defined in the training group
    :return: Boolean indicating whether the player matches the fitness requirement
    """
    # Check if the "fitness" key exists in the requirement
    if "fitness" not in requirement:
        # If the key does not exist in the requirement, return False
        return False

    # Get the `fitness` of the current player and the requirement
    player_fitness = player.get("fitness")
    range_str = requirement.get("fitness")

    # Split the range string and convert the resulting string values to floats
    lower, upper = map(float, range_str.split("-"))
    # Check if the player's fitness value is within the defined range (inclusive)
    return lower <= player_fitness <= upper


def matches_played(player, requirement):
    """
    Determines if the player's `played` attribute matches the requirements. Returns `True` if:
    - The `played` requirement is not defined in the `requirement` argument
    - The `played` attribute of the `player` argument matches the `played` requirement in the `requirement` argument

    :param player: A dictionary representing a player with keys:
        - 'played': a boolean representing whether the player has played or not
    :param requirement: A dictionary representing a requirement with keys:
        - 'played': a boolean representing whether the player should have played or not
    :return: Boolean indicating whether the player matches the requirement
    """
    # If the requirement does not have the "played" attribute, return True
    if "played" not in requirement:
        return True

    # Check if the player's "played" attribute matches the requirement's "played" attribute
    return player.get("played") == requirement.get("played")


def matches_position(player, requirement):
    """
    Determines whether the given player matches the position requirement.

    :param player: A dictionary representing a player with attributes such as 'position' and 'played'.
    :param requirement: A dictionary representing a requirement with attributes such as 'positions' and 'played'.
    :return: A boolean indicating whether the player matches the position requirement.
    """
    player_position = player.get("position")
    requirement_positions = requirement.get("positions")

    if not requirement_positions:
        return True

    return player_position in requirement_positions


def find_friendly_match_block_pattern(match_block_patterns: bs4.ResultSet):
    for match_block_pattern in match_block_patterns:
        style_element = match_block_pattern.select_one("style")
        if style_element and ".ol-training-weektable-matchblock-friendlies-info" in style_element.text:
            return match_block_pattern
    return None


def played_more_than_14_minutes(minutes):
    return minutes > 14


async def parse_player_info(session: aiohttp.ClientSession, location: str):
    # Get the BeautifulSoup object representing the training page
    soup = await get_training_soup(session, location)
    # in Order to test the code, you can use the following line instead of the above line
    # soup = get_training_soup_from_file("ONLINELIGA.de.html")

    # A dictionary to store the players who played more than 15 minutes
    players_played = {}

    # Get the match block patterns from the soup
    match_block_patterns = soup.select(".ol-training-weektable-matchblock-club")

    # A list of match types to consider
    match_types = ["league", "friendly"]

    # Loop through each match type
    for match_type in match_types:
        # Get the usage cells for the current match type
        usage_cells = get_usage_cells(match_block_patterns, match_type)

        # Parse the players who played more than 15 minutes in the current match type
        players_played.update(parse_players_played(usage_cells))

    # Return the players who played more than 15 minutes in all match types
    return players_played


def get_training_soup_from_file(file_path: str):
    with open(file_path, "r") as f:
        data = f.read()
    return BeautifulSoup(data, "html.parser")


async def parse_fitness_info(session: aiohttp.ClientSession, token: str, schedule_id: int, location: str):
    fitness_url = f"https://www.{location}/team/training/details?trainingScheduleId={schedule_id}"
    headers = {
        "user-agent": "Training Helper",
        "x-csrf-token": f"{token}",
    }
    response = await session.get(fitness_url, headers=headers)
    data = await response.text()
    soup = BeautifulSoup(data, "html.parser")

    players = {}
    player_rows = soup.select(".ol-training-playerdetails-table-row:not(.ol-training-playerdetails-table-head-row)")
    for row in player_rows:
        player_id = parse_training_player_id(row)

        width_regex = re.compile(r'width:(\d+\.\d+|\d+)%')

        layer_1_width = float(width_regex.search(
            row.select_one('.ol-value-bar-layer-1-base')['style']).group(1))
        layer_2_width = float(width_regex.search(
            row.select_one('.ol-value-bar-layer-2')['style']).group(1))

        fitness = layer_1_width + layer_2_width
        players[player_id] = fitness
    return players


async def get_training_soup(session: aiohttp.ClientSession, location):
    training_resp = await session.get(f"https://www.{location}/team/training")
    data = await training_resp.text()
    return BeautifulSoup(data, "html.parser")


def get_usage_cells(match_block_patterns, match_type):
    if match_type == "league":
        usage_cells = [row for row in match_block_patterns[0].select(".ol-training-weektable-matchblock-usages-row")
                       if row.select(".player-quick-overview-launcher")]
    else:
        usage_cells = [row for row in find_friendly_match_block_pattern(match_block_patterns).select(
            ".ol-training-weektable-matchblock-usages-row")
                       if row.select(".player-quick-overview-launcher")]
    return usage_cells


def parse_players_played(usage_cells):
    players_played = {}
    for cell in usage_cells:
        player_id = parse_training_player_id(cell)
        minutes_played = parse_minutes_played(cell)
        if played_more_than_14_minutes(minutes_played):
            players_played[player_id] = True
    return players_played


def parse_minutes_played(cell):
    # The minutes played is stored in a span element with class
    # "ol-training-weektable-matchblock-usages-cell-minutes0" or
    # "ol-training-weektable-matchblock-usages-cell-minutes1". or
    # "ol-training-weektable-matchblock-usages-cell-minutes2". The class name is different depending on whether the
    # player played the full match or not. If the player played the full match, the class name is
    # "ol-training-weektable-matchblock-usages-cell-minutes0". If the player did not play the full match,
    # the class name is "ol-training-weektable-matchblock-usages-cell-minutes1". If the player played less than 15
    # minutes, the class name is "ol-training-weektable-matchblock-usages-cell-minutes2".

    try:
        minutes_played = int(cell.
                             select_one(".ol-training-weektable-matchblock-usages-cell-minutes0").
                             text.replace("'", "", 1))
    except AttributeError:
        try:
            minutes_played = int(cell.
                                 select_one(".ol-training-weektable-matchblock-usages-cell-minutes1").
                                 text.replace("'", "", 1))
        except AttributeError:
            minutes_played = int(cell.
                                 select_one(".ol-training-weektable-matchblock-usages-cell-minutes2").
                                 text.replace("'", "", 1))
    return minutes_played


def update_player_list(players_list, played_dict, fitness_dict):
    for player in players_list:
        player_id = player['id']

        for k, v in played_dict.items():
            if int(k) == player_id:
                player["played"] = v

        for k, v in fitness_dict.items():
            if int(k) == player_id:
                player["fitness"] = v

    return players_list


def get_url_encoded_data(grouped_players, schedule_id, training_groups):
    url_encoded_data_set = []
    for group_id, v in grouped_players.items():
        group_name = ""
        for group in training_groups:
            if group["id"] == group_id:
                group_name = group["name"]
        base_data = {
            "trainingScheduleId": schedule_id,
            "trainingGroupId": group_id,
            "name": group_name,
        }
        base_data_encoded = urllib.parse.urlencode(base_data)

        player_ids = []
        for player in v:
            player_ids.append(player["id"])
        url_encoded_player_ids = urllib.parse.urlencode({'playerIds[]': player_ids}, doseq=True)
        url_encoded_data = f"{base_data_encoded}&{url_encoded_player_ids}"
        url_encoded_data_set.append(url_encoded_data)

    return url_encoded_data_set


async def main():
    args = parse_arguments()
    schedule_id, training_groups = get_training_schedule_details(args.schedule)
    players = get_players_test(args.players)

    headers = {"user-agent": "Training Helper"}
    async with aiohttp.ClientSession(headers=headers) as session:
        token = await login(session, args.location)
        players_fitness = await parse_fitness_info(session, token, schedule_id, args.location)
        players_that_played = await parse_player_info(session, args.location)
        players = update_player_list(players, players_that_played, players_fitness)
        grouped_players = group_players_by_training_requirements(training_groups, players)
        url_encoded_data_set = get_url_encoded_data(grouped_players, schedule_id, training_groups)
        training_url = f"https://www.{args.location}/team/training/group/save"
        training_group_post_headers = {
            "user-agent": "Training Helper",
            "content-type": "application/x-www-form-urlencoded; charset=UTF-8",
            "x-csrf-token": f"{token}", }
        for url_encoded_data in url_encoded_data_set:
            response = await session.post(training_url, data=url_encoded_data, headers=training_group_post_headers)
            await handle_tg_post_response(response)


if __name__ == '__main__':
    asyncio.run(main())
