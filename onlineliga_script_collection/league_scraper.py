#!/usr/bin/env python3
import argparse
import asyncio
import csv
import json
import re
import sys
import time
from argparse import Namespace
from datetime import datetime

import aiohttp
import bs4
import tqdm
from bs4 import BeautifulSoup
from shared.config_parser import config

start_time = time.time()


def parse_arguments() -> Namespace:
    # generate a parser instance
    parser = argparse.ArgumentParser(description=f"Scrape the OL website for league data."
                                                 f"\n This script was initially intended for the usage of an all-time "
                                                 f"league table, but can be used for other purposes as well.",
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('-f', '--file',
                        type=str,
                        help='path to the file containing the season_league dictionary',
                        required=False,
                        default="configuration_files/season_leagues.json")
    parser.add_argument("-o", "--output",
                        type=str,
                        help="directory where the output files will be stored",
                        required=False,
                        default="output")

    args = parser.parse_args()
    return args


def extract_season_leagues_by_location(json_file_path) -> dict:
    # Open the JSON file for reading
    with open(json_file_path, 'r') as json_file:
        data = json.load(json_file)

    # Initialize a dictionary to store season leagues by location
    season_leagues_dict = {}

    # Loop through entries and organize by location
    for entry in data["entries"]:
        location = entry.get("location")
        season_leagues_data = entry.get("season_leagues")

        # If the location is not in the dictionary, add it with an empty list
        if location not in season_leagues_dict:
            season_leagues_dict[location] = {}

        # Append the season_leagues_data to the location's dict
        season_leagues_dict[location].update(season_leagues_data)

    return season_leagues_dict


def fetch_ranking_rows(soup: bs4.BeautifulSoup) -> bs4.ResultSet:
    if soup is None:
        sys.exit(3)
    league_ranking_rows = soup.find_all(attrs={"id": "ol-td"})
    return league_ranking_rows


def parse_placement(league_ranking_row: bs4.BeautifulSoup):
    if league_ranking_row is None:
        return ""
    place_element = league_ranking_row.find(attrs={"class": "ol-table-number"})
    placement_split = place_element.text.split(".", 1)
    placement = placement_split[0]
    return placement


def parse_team_id(league_ranking_row: bs4.BeautifulSoup):
    if league_ranking_row is None:
        return ""
    element = league_ranking_row.find(attrs={"class": "ol-team-name"})
    if element is None:
        element = league_ranking_row.find(attrs={"class": "ol-team-name-inactive"})
    team_id_attr = element["onclick"]
    team_id = team_id_attr.split("userId : ", 1)[1].split(" }", 1)[0]
    return team_id


def parse_team_name(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    element = league_ranking_row.find(attrs={"class": "ol-team-name"})
    if element is None:
        element = league_ranking_row.find(attrs={"class": "ol-team-name-inactive"})
    team_name = element.text
    team_name = re.sub(r'\s+', ' ', team_name)
    return team_name


def parse_matches_played(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "ol-matchday-table-table-goals"})
    matches_played = elements[0].text
    return matches_played


def parse_goals(league_ranking_row: bs4.BeautifulSoup) -> (str, str):
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "ol-matchday-table-table-goals"})
    goals_text = elements[1].text
    goal_text_formatted = re.sub(r'\s+', '', goals_text)
    goals = goal_text_formatted.split(":", 1)
    return goals[0], goals[1]


def parse_goal_difference(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "ol-matchday-table-table-goals"})
    goal_difference = elements[2].text
    return goal_difference


def parse_points(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "ol-matchday-table-table-goals"})
    points = elements[3].text
    return points


def parse_wins(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "hidden-xs"})
    wins = elements[1].text
    return wins


def parse_draws(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "hidden-xs"})
    draws = elements[2].text
    return draws


def parse_losses(league_ranking_row: bs4.BeautifulSoup) -> str:
    if league_ranking_row is None:
        return ""
    elements = league_ranking_row.findAll(attrs={"class": "hidden-xs"})
    losses = elements[3].text
    return losses


def parse_league(soup: bs4.BeautifulSoup) -> (str, str):
    if soup is None:
        sys.exit()
    league_name_element = soup.find(attrs={"class": "ol-matchday-table-header2"})
    league_name_text = league_name_element.text
    league_name = league_name_text.split(" (", 1)[0]

    league_level = league_name.split(".", 1)[0]
    return league_name, league_level


async def fetch(session, url, season, league):
    async with session.get(url) as resp:
        teams = []

        page = await resp.text()
        soup = BeautifulSoup(page, "lxml")
        ranking_rows = fetch_ranking_rows(soup)
        league_name, league_level = parse_league(soup)

        for row in ranking_rows:
            season = season
            league_id = league
            league_name = league_name
            placement = parse_placement(row)
            team_name = parse_team_name(row)
            team_id = parse_team_id(row)
            matches_played = parse_matches_played(row)
            wins = parse_wins(row)
            draws = parse_draws(row)
            losses = parse_losses(row)
            goals_for, goals_against = parse_goals(row)
            goal_difference = parse_goal_difference(row)
            points = parse_points(row)
            team = [season, league_id, league_name, league_level, placement, team_id, team_name, matches_played,
                    wins, draws, losses, goals_for, goals_against, goal_difference, points]
            teams.append(team)
        return teams


async def bound_fetch(sem, session, url, season, league):
    async with sem:
        # When workers hit the limit, they'll wait for a second
        # before making more requests.
        if sem.locked():
            await asyncio.sleep(delay=config["lock_delay"])
        return await fetch(session, url, season, league)


async def fetch_all(file_path: str, output_dir: str):
    season_leagues_by_location = extract_season_leagues_by_location(file_path)
    for location, season_leagues in season_leagues_by_location.items():
        async with aiohttp.ClientSession() as session:
            semaphore = asyncio.Semaphore(config["concurrent_requests"])

            tasks = []
            for season, leagues in season_leagues.items():
                for league in range(leagues):
                    url = f'https://www.{location}/matchdaytable/leaguetable?leagueId={league + 1}&leagueLevel=1' \
                          f'&leagueMatchday=1&matchday=34&nav=true&navId=matchdayTable&s' \
                          f'eason={season}&stateId=1&type=1'
                    # print(url)
                    tasks.append(asyncio.ensure_future(
                        bound_fetch(semaphore, session, url, season, league + 1)))

            responses = []
            for f in tqdm.tqdm(asyncio.as_completed(tasks), total=len(tasks)):
                responses.append(await f)

            league_list = await asyncio.gather(*tasks)

            current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            file_name = f"{output_dir}/league_stats_{location}_{current_time}.csv"
            complete_file = open(f"{file_name}", "w", newline="", encoding="utf-8")
            writer = csv.writer(complete_file)
            writer.writerow(["season", "league_id", "league_name", "league level",
                             "placement", "team_id", "team_name", "matches", "wins",
                             "draws", "losses", "goals_for", "goals_against", "goal_difference",
                             "points"])
            for league in league_list:
                writer.writerows(league)
            complete_file.close()
            elapsed_time = time.time() - start_time
            print(f"\n league stats for {location} written to {file_name}")


def main():
    args = parse_arguments()

    if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(fetch_all(args.file, args.output))

    print(f"\n--- {time.time() - start_time} seconds --- elapsed in total")


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print(f"Keyboard Interrupt detected. Exiting...")
        exit(0)
