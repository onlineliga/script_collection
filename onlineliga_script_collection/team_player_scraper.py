#!/usr/bin/env python3
import argparse
import subprocess
import sys
from argparse import Namespace
from datetime import datetime
from pathlib import Path
import json
import csv
import shutil

from shared.location import select_location


def parse_arguments() -> Namespace:
    # generate a parser instance
    parser = argparse.ArgumentParser(
        description=f"This script scrapes the player data from the onlineliga. "
                    f"It was initially designed to fetch some important information for community "
                    f"organized tournaments.",
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-l ", "--location", type=str, help="DE/AT/CH/UK", required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])

    parser.add_argument("-t", "--team-ids", nargs="+", type=str, help="List of team IDs", required=True)
    parser.add_argument("-o", "--output",
                        type=str,
                        help="directory where the output files will be stored",
                        required=False,
                        default="output")

    args = parser.parse_args()

    if not args.location:
        args.location = select_location()
    return args


def create_tmp_dir() -> Path:
    relative_path = "tmp/ols"
    try:
        path = Path(str(Path.home()) + "/" + relative_path)
        path.mkdir(parents=True, exist_ok=True)
        return path
    except OSError as e:
        print(f"Error: {relative_path} : {e.strerror}")


def remove_tmp_dir(path: Path):
    try:
        shutil.rmtree(path)
    except OSError as e:
        print(f"Error: {path} : {e.strerror}")


def parse_tld(location: str) -> str:
    if location == "onlineliga.de":
        return "de"
    elif location == "onlineliga.ch":
        return "ch"
    elif location == "onlineliga.at":
        return "at"
    elif location == "onlineleague.co.uk":
        return "co.uk"
    else:
        raise ValueError("Unknown location")


def scrape_teams(team_ids: list, location: str, path: Path) -> list[str]:
    team_files = []

    for teamID in team_ids:
        subprocess.call([
            "ols",
            "-l", location,
            "team",
            "--id", teamID,
            "-o", f"{path}/{teamID}.json"
        ],
            stdout=sys.stdout,
            stderr=subprocess.STDOUT)
        team_files.append(f"{path}/{teamID}.json")
    return team_files


def write_to_csv(team_files, result_file_name):
    with open(result_file_name, "w", newline="") as complete_file:
        writer = csv.writer(complete_file)

        for team_file_path in team_files:
            with open(team_file_path, "r") as team_file:
                team_data = json.load(team_file)
                try:
                    for player in team_data["players"]:
                        player_data = [
                            team_data["team_id"],
                            player["id"],
                            player["name"],
                            player["overall"],
                            player["abilities"]["shooting"],
                            player["abilities"]["power"],
                            player["abilities"]["keeper_on_the_line"],
                            player["abilities"]["keeper_box"],
                            player["abilities"]["keeper_foot"],
                            player["positions"][0]
                        ]
                        writer.writerow(player_data)
                except TypeError as e:
                    print(f"Error: apparently no players in team {team_data['team_id']}. Skipping... "
                          f"Message: {e}")
                    continue


def main():
    args = parse_arguments()
    print(f"Selected location: {args.location}")
    location = parse_tld(args.location)
    print(f"Selected location: {location}")
    team_ids = args.team_ids

    path = create_tmp_dir()
    print(f"Created tmp directory: {path}")

    print(f"Scraping teams")
    team_files = scrape_teams(team_ids, location, path)
    print(f"Scraped {len(team_files)} team(s)")

    if team_files:
        current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_name = f"{args.output}/team_players_{location}_{current_time}.csv"
        print(f"Writing to {file_name}")
        write_to_csv(team_files, file_name)

    print(f"Cleanup tmp directory {path}")
    remove_tmp_dir(path)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print(f"Interrupted by user. Exiting...")
