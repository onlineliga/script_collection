#!/usr/bin/env python3
import asyncio
import aiohttp
import argparse

from argparse import Namespace
from bs4 import BeautifulSoup

from shared.login import login
from shared.location import select_location
from shared.http import fetch
from shared.server_info import fetch_info
from shared.friendly_parser import parse_accept_response, parse_friendly_element, parse_subpage


def parse_arguments() -> Namespace:
    # Generate a parser instance
    parser = argparse.ArgumentParser(description=f"Accept a friendly match",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-v", "--verbose", action="store_true",
                        help=f"Increase output verbosity",
                        required=False)
    parser.add_argument("-l", "--location", type=str,
                        help=f"DE/AT/CH/UK",
                        required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])
    parser.add_argument("-w ", "--week", type=str,
                        help=f"The week of the season"
                             f"(default: current)",
                        required=False,
                        default="current")
    parser.add_argument("-s", "--season", type=str,
                        help="The season of the league"
                             "(default: current)",
                        required=False,
                        default="current")
    parser.add_argument("-d", "--delay", type=int,
                        help=f"Amount of time in seconds to wait between receiving friendlies and accepting them"
                             f"(default: 10)",
                        default=10,
                        required=False)
    parser.add_argument("-t", "--team", type=str,
                        help=f"Team ID of a specific team that you want to play against"
                             f"(default: None)",
                        default=None,
                        required=False)
    parser.add_argument("-c", "--capacity", type=int,
                        help=f"Minimum capacity of the stadium of the team that you want to play against"
                             f"(default: 0)",
                        default=0,
                        required=False)
    parser.add_argument("-lmin", "--league_level_min", type=int,
                        help=f"Minimum league level of the team that you want to play against"
                             f"(default: 1)",
                        required=False,
                        default=1)
    parser.add_argument("-lmax", "--league_level_max", type=int,
                        help=f"Maximum league level of the team that you want to play against"
                             f"(default: 6)",
                        required=False,
                        default=6)
    parser.add_argument("-bt", "--blacklisted_teams", type=str,
                        help=f"Comma-separated list of team IDs that you don't want to play against"
                             f"(default: None)",
                        default=None,
                        required=False)

    args = parser.parse_args()

    if not args.location:
        args.location = select_location()
    return args


async def fetch_friendlies(session: aiohttp.ClientSession, location: str, season: str, week: str, page: int = 0) -> str:
    url = f"https://www.{location}/friendlies/requests"
    query_params = {
        "season": season,
        "week": week
    }
    if page > 0:
        query_params["subpage"] = page
        query_params["subload"] = 1

    headers = {
        "User-Agent": f"Mozilla/5.0 (X11; Linux x86_64) "
                      f"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }

    response = await fetch(session, url, params=query_params, headers=headers)

    if page > 0:
        response = parse_subpage(response)
    return response


def parse_friendly_requests(response: str, offset: int = 1) -> list:
    soup = BeautifulSoup(response, "lxml")
    friendly_elements = soup.select(".ol-friendly-requests-table-row")
    friendlies = []
    if len(friendly_elements) <= 1:
        return friendlies

    for friendly_element in friendly_elements[offset:]:
        friendly = parse_friendly_element(friendly_element)

        friendlies.append(friendly)
    return friendlies


def filter_friendlies(friendlies, args):
    filtered_friendlies = []

    for friendly in friendlies:
        team_id = friendly["team_id"]
        league_level = friendly["league_level"]
        capacity = friendly["capacity"]
        accept_button = friendly["accept_button"]

        # Apply filters based on command line arguments
        if args.team and args.team != team_id:
            continue
        if args.capacity and args.capacity > capacity:
            continue
        if args.league_level_min and args.league_level_min > league_level:
            continue
        if args.league_level_max and args.league_level_max < league_level:
            continue
        if args.blacklisted_teams and str(team_id) in args.blacklisted_teams.split(','):
            continue
        if accept_button != "enabled":
            continue

        # If all filters pass, add the friendly to the filtered list
        filtered_friendlies.append(friendly)

    return filtered_friendlies


async def accept_friendly(session: aiohttp.ClientSession, location: str, friendly_id: str, token: str) -> bool:
    url = f"https://www.{location}/friendlies/request/accept"
    headers = {
        "User-Agent": f"Mozilla/5.0 (X11; Linux x86_64) "
                      f"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
        "x-csrf-token": f"{token}",
    }

    payload = {
        "id": friendly_id,
        "force": 1
    }

    try:
        response = await session.post(url, data=payload, headers=headers)
        response_text = await response.text()
        friendly_accept_message = parse_accept_response(response_text)
        return friendly_accept_message["success"]
    except Exception as e:
        print(f"Error accepting friendly: {e}")
        return False


async def main():
    args = parse_arguments()
    location = args.location
    season = args.season
    week = args.week
    verbose = args.verbose
    delay = args.delay

    async with aiohttp.ClientSession() as session:
        if season == "current" or week == "current":
            info = await fetch_info(session, location)
            season = season if season != "current" else info["season"]
            week = week if week != "current" else info["week"]

        token = await login(session, location)
        while True:
            page = 0
            response = await fetch_friendlies(session, location, season, week)
            friendlies = parse_friendly_requests(response)
            friendly_amount = len(friendlies)

            # When there are more than 20 friendlies, we need to fetch the next page
            while friendly_amount >= 20 and friendly_amount % 20 == 0:
                page = page + 1
                response = await fetch_friendlies(session, location, season, week, page)
                # use offset=0 table header is not included in the response
                friendlies = friendlies + parse_friendly_requests(response, offset=0)
                friendly_amount = len(friendlies)

            if verbose:
                print(f"All Friendlies: ({friendly_amount})")
                for friendly in friendlies:
                    print(friendly)

            filtered_friendlies = filter_friendlies(friendlies, args)

            print("Filtered Friendlies:")
            for friendly in filtered_friendlies:
                print(friendly)

            if filtered_friendlies:
                friendly = filtered_friendlies[0]
                friendly_id = friendly["friendly_id"]
                success = await accept_friendly(session, location, friendly_id, token)
                if success:
                    print(f"Successfully accepted friendly {friendly_id}"
                          f"\n{friendly}"
                          f"\nFinishing script")
                    exit(0)
                else:
                    print(f"Failed to accept friendly {friendly_id}"
                          f"\n{friendly}"
                          f"\nRetrying in {delay} seconds")

            print(f"Sleeping for {delay} seconds")
            await asyncio.sleep(delay)


if __name__ == "__main__":
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("KeyboardInterrupt")
        exit(0)
